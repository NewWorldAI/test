package net.radionica.hello.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;	

import org.hibernate.validator.constraints.UniqueElements;

@Entity
public class User {
    @Id @GeneratedValue
    @Column(name = "id")
    private int _id;
    
    @UniqueElements
    @Column(name = "username")
    private String _username;
    
    public User() {}

    public int get_Id() {
        return _id;
    }

    public void setId(int id) {
       _id = id;
    }

    public String getUsername() {
        return _username;
    }

    public void setUsername(String username) {
        _username = username;
    }
    
    
}
