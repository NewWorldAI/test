package net.radionica.hello.dao;

import org.springframework.data.repository.CrudRepository;

import net.radionica.hello.model.User;

public interface UserRepository extends CrudRepository<User, Integer>  {
    
    User findBy_username(String username);
}