package net.radionica.hello.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import net.radionica.hello.model.User;
import net.radionica.hello.service.UserService;

@RestController
public class UserController {
    @Autowired
    private UserService _userService;

    private UserService get_userService() {
        return _userService;
    }

    @RequestMapping(value = "/saveUser", method = RequestMethod.POST)
    public void saveUser(@RequestBody User user) {
	get_userService().saveUser(user);
    }
    
    @RequestMapping(value = "/find/userId={id}", method = RequestMethod.GET)
    public User findById(
	    @PathVariable int id) {
	
	User user = get_userService().findById(id);
	return user;
    }
    
    @RequestMapping(value = "/find/username={username}", method = RequestMethod.GET)
    public User findByUsername(
	    @PathVariable String username) {
	
	User user = get_userService().findByUsername(username);
	return user;
    }
    
    
}
