package net.radionica.hello.service;
import net.radionica.hello.dao.UserRepository;
import net.radionica.hello.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserRepository _userRepository;
    
    public User findById(int id) {
	return getUserRepository().findById(id).get();
    }
    
    public User findByUsername(String username) {
	return getUserRepository().findBy_username(username);
    }
    
    public void saveUser(User user) {
	getUserRepository().save(user);
    }

    private UserRepository getUserRepository() {
        return _userRepository;
    }
    
    
}
