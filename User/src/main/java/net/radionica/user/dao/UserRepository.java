package net.radionica.user.dao;

import org.springframework.data.repository.CrudRepository;

import net.radionica.user.model.User;

public interface UserRepository extends CrudRepository<User, Integer>  {
    
    User findBy_username(String username);
}