package net.radionica.user.controller;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import net.radionica.user.model.User;
import net.radionica.user.service.UserService;

@RestController
public class UserController {
    @Autowired
    private UserService _userService;

    private UserService get_userService() {
        return _userService;
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public void saveUser(@RequestBody User user) {
    	user.setPassword(DigestUtils.sha1Hex(user.getPassword()));
    	get_userService().saveUser(user);
    }
    
    @RequestMapping(value = "/users/{username}", method = RequestMethod.GET)
    public User findByUsername(
	    @PathVariable String username) {
	
    	User user = get_userService().findByUsername(username);
    	return user;
    }
    
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public Iterable<User> findByUsername() {
    	return get_userService().findAll();
    }
}
