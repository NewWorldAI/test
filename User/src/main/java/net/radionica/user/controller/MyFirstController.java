package net.radionica.user.controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyFirstController {
    
    @RequestMapping("/hello")
    public String test() {
	return "Hello World";
    }
}
