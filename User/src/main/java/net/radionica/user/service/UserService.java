package net.radionica.user.service;
import net.radionica.user.dao.UserRepository;
import net.radionica.user.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserRepository _userRepository;
    
    public Iterable<User> findAll() {
    	return getUserRepository().findAll();
    }
    
    public User findByUsername(String username) {
    	return getUserRepository().findBy_username(username);
    }
    
    public void saveUser(User user) {
    	getUserRepository().save(user);
    }

    private UserRepository getUserRepository() {
        return _userRepository;
    }
    
    
}
